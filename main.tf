terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }
  }
  backend "gcs" {
    // alterar nome do bucket
    bucket = "terraform-spekter"
    prefix = "terraform/state"
  }
  required_version = ">= 0.14"
}

provider "google-beta" {
  //  credentials = file("<NAME>.json")
  project = var.project_id
  region  = var.region
  zone    = var.zone
}