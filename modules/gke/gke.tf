# GKE cluster
resource "google_container_cluster" "cluster" {
  provider                 = google-beta
  project                  = var.project_id
  name                     = "${var.project_id}-gke"
  location                 = var.zone
  remove_default_node_pool = true
  initial_node_count       = var.gke_initial_node_count

  enable_binary_authorization = false
  enable_kubernetes_alpha     = false
  enable_legacy_abac          = false
  enable_shielded_nodes       = true
  enable_intranode_visibility = false
  default_max_pods_per_node   = 110
  logging_service             = "logging.googleapis.com/kubernetes"
  monitoring_service          = "monitoring.googleapis.com/kubernetes"
  networking_mode             = "VPC_NATIVE"
  network                     = "${var.project_id}-vpc"
  subnetwork                  = "${var.project_id}-subnet"

  addons_config {
    horizontal_pod_autoscaling {
      disabled = false
    }
    http_load_balancing {
      disabled = false
    }
    network_policy_config {
      disabled = true
    }
  }

  cluster_autoscaling {
    enabled = false
  }

  ip_allocation_policy {
    cluster_ipv4_cidr_block  = ""
    services_ipv4_cidr_block = ""
  }

  maintenance_policy {
    recurring_window {
      start_time = "2022-01-01T02:00:00Z"
      end_time   = "2022-01-01T08:00:00Z"
      recurrence = "FREQ=WEEKLY;BYDAY=SU,SA"
    }
  }

  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  network_policy {
    enabled = false
  }

  release_channel {
    channel = "STABLE"
  }

}