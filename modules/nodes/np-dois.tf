# Separately Managed Node Pool

resource "google_container_node_pool" "np-dois" {
  project            = var.project_id
  # depends_on         = [google_container_cluster.cluster]
  name               = "np-dois"
  location           = var.zone
  cluster            = "${var.project_id}-gke"
  version            = var.gke_version
  initial_node_count = var.gke_initial_node_count

  node_config {
    machine_type = "e2-custom-4-4096"
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/userinfo.email"
    ]

    preemptible = false
    image_type   = "COS_CONTAINERD"
    disk_type    = "pd-standard"
    disk_size_gb = 25

    tags = [
      "gke-node",
      "${var.project_id}-gke"
    ]

    # node_pools_taints = {
    #   key    = "nodepool"
    #   value  = standard
    #   effect = "NO_SCHEDULE"
    # }
    # node_pools_taints = {
    #   all = []

    #   nodepool = [
    #     {
    #       key    = "nodepool"
    #       value  = standard
    #       effect = "NO_SCHEDULE"
    #     },
    #   ]
    # }

    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels = {
      "cloud.google.com/gke-preemptible" = false
      "nodeTypePoolSpec"                 = var.gke_node_selector
    }
  }

  # Autoscaling config.
  autoscaling {
    min_node_count = var.environment == "production" ? 1 : 1
    max_node_count = var.environment == "production" ? 35 : 1
  }

  # Management Config
  management {
    auto_repair  = true
    auto_upgrade = true
  }

}