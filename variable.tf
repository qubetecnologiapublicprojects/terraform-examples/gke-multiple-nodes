variable "project_id" {
  type        = string
  description = "Digite o ID do projeto no GCP"
}

variable "region" {
  type        = string
  description = "Digite a região onde será implementada a infra. Ex.: us-east1"
}

variable "zone" {
  type        = string
  description = "Digite a zona de disponibilidade onde será implementada a infra. Ex.: us-east1-c"
}

variable "gke_initial_node_count" {
  default = 1
  type    = number
}

variable "gke_version" {
  type    = string
  description = "Digite a versão do GKE que será implementada. Ex.: 1.20.11-gke.1300 "
}

variable "gke_max_pods_per_node" {
  default = 110
  type    = number
}

variable "gke_node_selector" {
  default = "default"
  type    = string
}

variable "environment" {
  type        = string
  description = "Digite o environment, staging, development ou production. Se environment = production, então o auto-scale de nodes será ativado"
}